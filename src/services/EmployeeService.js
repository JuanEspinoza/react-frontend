import axios from "axios";

const EMPLOYE_BASE_REST_API_URL = "https://spp-1.fly.dev/api/v1/employees";

class EmployeeService {
    getAllEmployees() {
        return axios.get(EMPLOYE_BASE_REST_API_URL);
    }

    createEmployee(employee) {
        return axios.post(EMPLOYE_BASE_REST_API_URL, employee);
    }

    getEmployeeById(employeeId) {
        console.log("Dentro del metodo getEmployeeId");
        console.log(EMPLOYE_BASE_REST_API_URL + '/' + employeeId);
        return axios.get(EMPLOYE_BASE_REST_API_URL + '/' + employeeId);
    }

    updateEmployee(employeeId, employee) {
        return axios.put(EMPLOYE_BASE_REST_API_URL + '/' + employeeId, employee);
    }

    deleteEmployee(employeeId, employee) {
        return axios.delete(EMPLOYE_BASE_REST_API_URL+'/'+employeeId);
    }
}

export default new EmployeeService(); 