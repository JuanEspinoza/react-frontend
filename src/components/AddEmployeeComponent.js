import React, { useEffect, useState } from 'react'
import EmployeeService from '../services/EmployeeService';
import {Link, useNavigate, useParams} from 'react-router-dom';


export const AddEmployeeComponent = () => {

    const[firstName, setFirstName] = useState('');
    const[lastName, setLastName] = useState('');
    const[emailId, setEmailId] = useState('');
    const navigate = useNavigate();
    const {id} = useParams();

    const saveOrUpdateEmployee = (e)=>{
            e.preventDefault();
            
            const employee={firstName,lastName,emailId};

            if(id){
                EmployeeService.updateEmployee(id,employee).then((response)=>{
                    navigate('/employees');
                }).catch(error=> {
                    console.log(error);
                });
            }else{
                console.log(employee);
                EmployeeService.createEmployee(employee).then((reponse)=>{    
                    navigate('/employees');
                    }).catch(error=>{
                        console.log(error);
                } );
            }
    }

   useEffect(()=>{
        EmployeeService.getEmployeeById(id).then((response)=>{
            setFirstName(response.data.firstName);
            setLastName(response.data.lastName);
            setEmailId(response.data.emailId);
        }).catch(error=>{
            console.log(error);
        })
   },[]); 

    const title = () =>{
        console.log(id);
        if(id){
            return <h2 className='text-center'>Update Employee</h2>
        }else{
            return <h2 className='text-center'>Add Employee</h2>
        }
    }

    return (
        <div>
            <br></br>
            <div className='container'>
                <div className='row'>
                    <div className='car col-md-6 offset-md-3 offset-md-3'>
                        <h2 className='text-center'>{title()}</h2>
                        <div className='card-body'>
                            <form>
                                <div className='form-group  mb-2'>
                                    <label className='form-label'>First Name:</label>
                                    <input type="text" placeholder='Enter first name'
                                    name='first-name' className='form-control' value={firstName}
                                    onChange={(e)=> setFirstName(e.target.value)}/>
                                </div>
                                <div className='form-group  mb-2'>
                                    <label className='form-label'>Last Name:</label>
                                    <input type="text" placeholder='Enter last name'
                                    name='last-name' className='form-control' value={lastName}
                                    onChange={(e)=> setLastName(e.target.value)}/>
                                </div>
                                <div className='form-group  mb-2'>
                                    <label className='form-label'>Email Id:</label>
                                    <input type="text" placeholder='Enter first name'
                                    name='email-id' className='form-control' value={emailId}
                                    onChange={(e)=> setEmailId(e.target.value)}/>
                                </div>
                                <button className='btn btn-success' onClick={(e)=>saveOrUpdateEmployee(e)}>Save</button>
                                <Link to='/employees' className='btn btn-danger'>Cancel</Link>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
