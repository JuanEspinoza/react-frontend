import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ListEmployeeComponent } from './components/ListEmployeeComponent';
import { HeaderComponent } from './components/HeaderComponent';
import { FooterComponent } from './components/FooterComponent';
import { AddEmployeeComponent } from './components/AddEmployeeComponent';

function App() {
  return (
    <div>
      <Router>
        <HeaderComponent/>
        <div className="container">
          <Routes>
            <Route exact path='/' element={<ListEmployeeComponent/>}></Route>
            <Route exact path='/employees' element={<ListEmployeeComponent/>}></Route>
            <Route exact path='/add-employee' element={<AddEmployeeComponent/>} />
            <Route exact path='/edit-employee/:id' element={<AddEmployeeComponent/>} />
          </Routes>
        </div>
        <FooterComponent/>
      </Router>
    </div>
    /*<div className="App">
      <HeaderComponent/>
      <ListEmployeeComponent />
      <FooterComponent/> 
    </div>*/
  );
}

export default App;
